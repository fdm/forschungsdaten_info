# Einrichtungen und FDM-Ansprechpartner

## Universitäten und Hochschulen

### Universität Hamburg (UHH)
Zuständige Einrichtung:
- [Zentrum für nachhaltiges Forschungsdatenmanagement (ZFDM)](https://www.fdm.uni-hamburg.de/)  
Monetastraße 4  
20146 Hamburg  
Kontakt: forschungsdaten@uni-hamburg.de

### Technische Universität Hamburg (TUHH)
Zuständige Einrichtung:
- [Universitätsbibliothek, Abteilung Digitale Dienste](https://www.tub.tuhh.de/publizieren/forschungsdaten/)  
Am Schwarzenberg-Campus 1  
21073 Hamburg  
Kontakt: [forschungsdaten@tuhh.de](mailto:forschungsdaten@tuhh.de)

### Hochschule für Angewandte Wissenschaften Hamburg (HAW)

### HafenCity Universität Hamburg (HCU)

### Helmut-Schmidt-Universität (HSU)

### Bucerius Law School

## Weitere Forschungseinrichtungen und Bibliotheken

### Akademie der Wissenschaften in Hamburg (AdWHH)

### ZBW - Leibniz-Informationszentrum Wirtschaft

### Deutsches Klimarechenzentrum (DKRZ)

### Deutsches Elektronen-Synchrotron (DESY)

### Staats- und Universitätsbibliothek Hamburg (SUB)

